import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React, { useEffect } from "react";
import _ from "lodash";
import { RouteNames } from "../../shared/helpers/routeName";
import MainStackScreen from "./MainStackScreen";
import AuthStackScreen from "./AuthStackScreen";
import { useSelector } from "react-redux";
import { RootState } from "../redux/reducers";
import { IReducer } from "src/data/interfaces/common";
import { UserModel } from "src/data/models/UserModel";
import SplashScreen from "react-native-splash-screen";

const RootStack = createNativeStackNavigator();

const RootNavigator = () => {
  const userReducer: IReducer<UserModel> = useSelector(
    (state: RootState) => state.userReducer.loginReducer
  );

  useEffect(() => {
    setTimeout(() => {
      SplashScreen.hide();
    }, 1000);
  }, []);

  return (
    <RootStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      {!!userReducer.data?.token ? (
        <RootStack.Screen
          name={RouteNames.MainStack}
          component={MainStackScreen}
        />
      ) : (
        <RootStack.Screen
          name={RouteNames.AuthStack}
          component={AuthStackScreen}
        />
      )}
    </RootStack.Navigator>
  );
};

export default RootNavigator;
