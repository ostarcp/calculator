import { isEmpty } from "lodash";
import React, { useCallback, useState } from "react";
import { SafeAreaView, StyleSheet, TouchableOpacity, View } from "react-native";
import { Host } from "react-native-portalize";
import { Colors } from "react-native/Libraries/NewAppScreen";
import { useDispatch } from "react-redux";
import { AppButton } from "src/modules/components/appButton";
import { AppInput } from "src/modules/components/appInput";
import { AppText } from "src/modules/components/appText";
import { logoutUser } from "src/modules/redux/actions/user";
import theme from "src/shared/theme";

const OPERRATE = ["+", "-", "*", "/", "(", ")"];
let operators = "+-*/";
let numbers = "0123456789";

const HomeScreen = () => {
  const dispatch = useDispatch();
  const [input, setinput] = useState("(2+5*(3-1)/4)-(2+4/(9-3))*2");
  const onLogout = useCallback(() => {
    dispatch(logoutUser());
  }, [dispatch]);

  const calculate = (numb1: number, numb2: number, operator: string) => {
    switch (operator) {
      case "+":
        return numb2 + numb1;
      case "-":
        return numb2 - numb1;
      case "*":
        return numb2 * numb1;
      case "/":
        return numb2 / numb1;

      default:
        return 0;
    }
  };

  const prec = (c: string) => {
    if (["/", "*"].includes(c)) return 2;
    if (["+", "-"].includes(c)) return 1;
    return -1;
  };

  const convertInfixToPostfixExpression = (s: string) => {
    // convertInfixToPostfixExpression
    let stack = [];
    let result: string = "";

    for (let i = 0; i < s.length; i++) {
      let c = s[i];

      if (numbers.includes(c)) result += c;
      else if (c == "(") stack.push("(");
      else if (c == ")") {
        while (stack[stack.length - 1] != "(") {
          result += stack[stack.length - 1];
          stack.pop();
        }
        stack.pop();
      } else {
        while (
          stack.length != 0 &&
          prec(s[i]) <= prec(stack[stack.length - 1])
        ) {
          result += stack[stack.length - 1];
          stack.pop();
        }
        stack.push(c);
      }
    }

    while (stack.length != 0) {
      result += stack[stack.length - 1];
      stack.pop();
    }

    return result;
  };

  const useEval = (x: string) => {
    "use strict";
    try {
      return eval(x);
    } catch (error) {
      return "Invalid operation";
    }
  };

  const calculateInfixExpression = () => {
    if (isEmpty(input)) return;
    const resultStack: number[] = [];
    const postfixExpression = convertInfixToPostfixExpression(input);

    postfixExpression.split("").forEach((el: any) => {
      let x = el.toString();

      if (numbers.includes(x)) {
        resultStack.push(+x);
        return;
      }
      if (operators.includes(x)) {
        let numb1 = resultStack.pop()!;
        let numb2 = resultStack.pop()!;
        let result = calculate(numb1, numb2, x);
        resultStack.push(result);
        return;
      }
    });
    if (isNaN(resultStack[0])) {
      console.log("eval");
      const value = useEval(input).toString();
      setinput(value);
      return;
    }
    setinput(resultStack[0].toString());
  };

  const handleInput = (v: string) => {
    setinput((pre) => `${pre}${v.toString()}`);
  };

  const renderButton = (key: any) => {
    return (
      <TouchableOpacity
        style={styles.buttonStyle}
        onPress={() => handleInput(key)}
        activeOpacity={0.6}
      >
        <AppText style={{ textAlign: "center" }} children={key} />
      </TouchableOpacity>
    );
  };

  const renderCalculate = () => {
    return (
      <View
        style={{
          flexDirection: "row",
        }}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "flex-start",
            alignItems: "center",
            flexWrap: "wrap",
            width: "70%",
          }}
        >
          {[...Array(10).keys()].map((item: any) => renderButton(item))}
        </View>
        <View
          style={{
            width: "30%",
          }}
        >
          {OPERRATE.map((el: string) => (
            <TouchableOpacity
              style={styles.buttonOpstyle}
              onPress={() => handleInput(el)}
              activeOpacity={0.6}
            >
              <AppText style={{ textAlign: "center" }} children={el} />
            </TouchableOpacity>
          ))}
          <TouchableOpacity
            style={styles.buttonOpstyle}
            onPress={() => setinput("")}
            activeOpacity={0.6}
          >
            <AppText style={{ textAlign: "center" }} children={"AC"} />
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  return (
    <Host>
      <SafeAreaView style={{ backgroundColor: Colors.white, flex: 1 }}>
        {/* <StatusBar barStyle={"light-content"} /> */}
        <View style={{ flex: 1, padding: 10 }}>
          <AppText
            style={{ textAlign: "center", marginVertical: 10 }}
            children="Calculate"
          />
          <AppInput
            value={input}
            editable={false}
            onPressRight={() =>
              setinput((pre) => pre.substring(0, pre.length - 1))
            }
            inputContainerStyle={styles.input}
          />
          {renderCalculate()}
          <AppButton
            style={styles.btn}
            text="="
            onPress={calculateInfixExpression}
          />
        </View>
        <AppButton text="Log out" onPress={onLogout} />
      </SafeAreaView>
    </Host>
  );
};

const styles = StyleSheet.create({
  input: {
    borderWidth: 3,
    borderColor: theme.color.gray88,
  },
  buttonStyle: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    padding: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    width: 60,
    height: 60,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
    borderRadius: 50,
  },
  buttonOpstyle: {
    backgroundColor: "white",
    padding: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,
  },
  btn: {
    marginTop: 20,
    borderWidth: 0,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
  },
});

export default HomeScreen;
