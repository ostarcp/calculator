import { baseUrl, urls } from "src/data/api/resource";
import { IPostLoginRequest } from "src/data/interfaces/request/user/IPostLoginRequest";
import ResponseModel from "src/data/models/common/responseModel";
import { UserModel } from "src/data/models/UserModel";
import UserRepository from "src/data/repositories/user";
import AxiosMockAdapter from "__tests__/mocks/AxiosMockAdapter";

describe('Login Email', () => {
    const body: IPostLoginRequest = {
        grantType: "password",
        email: 'liffu@yopmail.com',
        password: 'Admin123!',
    }

    it('Given success loginEmail response, loginEmail should success', done => {
        const mockApi = new AxiosMockAdapter(`${baseUrl}${urls.loginEmail}`);
        const mockResponse = require('../../assets/user/login_email_response.json');
        mockApi.onPost().reply(200, mockResponse);

        UserRepository.login(body).then((res: ResponseModel<UserModel>) => {
            const data = res.data;
            expect(typeof data?.token).toBe('string');
            done();
        }).catch((e) => fail('Should be success with response'));
    });

})